require_relative 'user_record_container.rb'
require_relative 'input_reader'
require_relative 'user'

class UserRecordInitializer
	def initialize(input_reader)
      @input_reader=input_reader  
    end

    def populate_user_data
       
      user_data_array=@input_reader.read_file
      user_data_array.each do |row|
        user_obj=User.new((get_user_name_from_row (row)),(get_password_from_row (row)),(get_actions_permitted_from_row(row))) 
        UserRecordContainer.instance.add_records(user_obj)
      end
    end 
    
    def get_user_name_from_row (row)
        return row [0].to_s rescue ' '
    end
          
    def get_password_from_row (row)
        return row[1].to_s rescue ' '
    end
      
    def get_actions_permitted_from_row (row)
        return row[2].to_s rescue ' ' 
    end

    
  private :get_user_name_from_row, :get_password_from_row, :get_actions_permitted_from_row
 
end	