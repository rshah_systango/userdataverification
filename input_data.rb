class InputData
	def initialize (name,password,actions_performed)
		@name=name
		@password=password
		@actions_performed=actions_performed.chomp.split(",")
	end	

	def get_name
		@name
	end
	
	def get_password
		@password		
	end
	
	def get_actions_performed
		return @actions_performed
	end		

end	