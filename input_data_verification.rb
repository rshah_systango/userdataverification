require 'pry'
require_relative 'user_record_container'
require_relative 'input_data'
require_relative 'user'	
class InputDataVerification
	
	def initialize (input_data)
		@input_data=input_data
		@user_record_array=UserRecordContainer.instance.get_record_array
	end

	def check_user_name
		entered_name=@input_data.get_name
		@user_record_array.each do |user_record|
			if(entered_name==(user_record.get_name))
				return user_record
			end
		end	
		return false
	end
	
	def check_password (user_record)
		entered_password=@input_data.get_password
			if(entered_password==user_record.get_password)
				return true
			end
		return false
	end

	def check_actions_permitted (user_record) 
		
		all_actions_not_permitted ||=[]
		actions_performed=@input_data.get_actions_performed
		actions_performed.each do |action_perform|
			invalid_action=''				
			action_valid=false
			user_record.get_action_permitted.each do |action_permit|
				if( action_perform == action_permit)
					action_valid=true
					break
				else
					invalid_action=action_perform
				end
			end
			if(!action_valid)
					all_actions_not_permitted << [invalid_action,false]
			end
		end		
		return all_actions_not_permitted
	end
end									