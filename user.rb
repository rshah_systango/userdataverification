class User
  def initialize (name,password,actions_permitted)
    @name=name
    @password=password
    @actions_permitted=actions_permitted.chomp.split(",") 
  end
  
  def get_name
   return @name
  end

  def get_password
   return @password
  end
  
  def get_action_permitted
   return @actions_permitted
  end    

end
