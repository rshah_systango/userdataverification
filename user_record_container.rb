require 'singleton'
require_relative 'user'
class UserRecordContainer  

 include Singleton
    def initialize
      @user_data_array ||= []
    end

    def add_records(user_obj)
      @user_data_array << user_obj
    end

    def get_record_array
      @user_data_array
    end  
end