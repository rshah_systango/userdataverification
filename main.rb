require_relative 'user_access_verifier.rb'
module Outputs
WRONG_NAME=' user name is incorrect'
WRONG_PASSWORD=' password is incorrect'
ACTION_MESSAGE=' are action results'
end
module FileName
USER_DATA_FILENAME='user_data.csv'
INPUT_FILENAME='input.csv'
end

puts "Write the input file name"
input_file_name=$stdin.read

user_access_verifier=UserAccessVerifier.new(FileName::USER_DATA_FILENAME,input_file_name=FileName::INPUT_FILENAME)
user_access_verifier.process()
