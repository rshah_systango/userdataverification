require_relative 'file_validator.rb'
require 'csv'
require	'pry'

class InputReader
	
  def initialize (input_file_name)
		@input_filename=input_file_name

    if(FileValidator.check_file_exist(@input_filename) and FileValidator.check_read_permission(@input_filename))
      puts 'file is readable'
    else
      raise '#{@input_filename} not found'
    end 	
  end	

  def read_file
    row_counter=0
    row_arr=Array.new
    CSV.foreach(@input_filename, converters: :numeric) do |row|
      if (row_counter>=1)
        row_arr<<row                           
      end
        row_counter=row_counter+1
      end         
    return row_arr
  end 

end	