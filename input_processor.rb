require_relative 'input_reader'
require_relative 'user_record_container'
require_relative 'input_data_verification'
require_relative 'input_data'
require_relative 'user'
class InputProcessor
	def initialize(input_reader)
      @input_reader=input_reader  
      @processing_result ||= []
  end

  def process_input_data
    input_data_array=@input_reader.read_file
    input_data_array.each do |row|
      actions_performed_result ||= []
      input_obj=InputData.new((get_user_name_from_row (row)),(get_password_from_row (row)),(get_actions_performed_from_row (row))) 
      input_data_verification=InputDataVerification.new(input_obj) 
        if(verified_username_object=input_data_verification.check_user_name)
          if (password_verification_result=input_data_verification.check_password(verified_username_object))
            if (actions_performed_result=input_data_verification.check_actions_permitted(verified_username_object))
                  @processing_result << [input_obj.get_name,actions_performed_result,Outputs::ACTION_MESSAGE]
            end  
          else
              @processing_result << [input_obj.get_password+Outputs::WRONG_PASSWORD]
          end
        else
            @processing_result << [input_obj.get_name+Outputs::WRONG_NAME]
        end 
    end
  end 

  def get_processing_result
    return @processing_result
  end  
    
  def get_user_name_from_row (row)
      return row [0].to_s rescue ' '
  end
        
  def get_password_from_row (row)
      return row[1].to_s rescue ' '
  end
    
  def get_actions_performed_from_row (row)
      return row[2].to_s rescue ' ' 
  end
  private :get_user_name_from_row, :get_password_from_row, :get_actions_performed_from_row

end	